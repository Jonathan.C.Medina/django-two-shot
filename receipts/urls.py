from django.contrib import admin
from django.urls import path
from receipts.views import receipt, create_receipt, category_list
from receipts.views import create_expense_category, account_list
from receipts.views import create_account_name

urlpatterns = [
    path("", receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_expense_category, name="create_category"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account_name, name="create_account")
]
