from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceiptForm, CreateExpenseCategoryForm
from receipts.forms import CreateAccountForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/receipt.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()
    context = {
            "form": form,
        }
    return render(request, "receipts/create_receipt.html", context)

@login_required
def category_list(request):
    category_lists = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_lists": category_lists,
    }
    return render(request, "receipts/category_list.html", context)

@login_required
def account_list(request):
    account_lists = Account.objects.filter(owner=request.user)
    context = {
        "account_lists": account_lists,
    }
    return render(request, "receipts/account_list.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account_name(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account_name = form.save(False)
            account_name.owner = request.user
            account_name.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
