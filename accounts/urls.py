from accounts.views import user_login, signup, user_logout
from django.urls import path

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup")
]
